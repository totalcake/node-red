var http = require('http');
var express = require("express");
var RED = require("node-red");
var path = require('path');
require('dotenv').config()

// Create an Express app
var app = express();

// Add a simple route for static content served from 'public'
app.use("/",express.static("dist"));

// Create a server
var server = http.createServer(app);

// Create the settings object - see default settings.js file for other options
var settings = {
    httpAdminRoot:"/red",
    httpNodeRoot: "/api",
    userDir: path.join( __dirname, "red"),
    flowFile: "flows",
    functionGlobalContext: { 
        cuid:require('@paralleldrive/cuid2'),
        path:require('path'),
        fs:require('fs'),
        simpleGit:require('simple-git')
    }    // enables global context
};

// Initialise the runtime with a server and settings
RED.init(server,settings);

// Serve the editor UI from /red
app.use(settings.httpAdminRoot,RED.httpAdmin);

// Serve the http nodes UI from /api
app.use(settings.httpNodeRoot,RED.httpNode);

server.listen(process.env.SERVER_PORT || 9000);

// Start the runtime
RED.start();